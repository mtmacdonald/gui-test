import React from 'react';
import { storiesOf } from '@storybook/react';
import Example from '../components/Example/Example.js';

storiesOf('Basic|Example', module)
  .add('default', () => (
      <Example />
  ));
