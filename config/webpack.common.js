const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const node = require('./webpack.common.node.js');

module.exports = {
  resolve: {
    alias: {
      '~common': path.resolve(__dirname, '../common/'),
      '~components': path.resolve(__dirname, '../client/components'),
      '~client': path.resolve(__dirname, '../client/'),
    }
  },
  entry: {
    app: './client/app.jsx',
    'adm/index': './admclient/admclient.jsx'
  },
  output: {
    path: path.resolve(__dirname, '../www'),
    publicPath: '/',
    filename: '[name].[contenthash].js'
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: 'wwwsrc/app.html',
      filename: './app.html',
      favicon: 'wwwsrc/favicon.png',
      chunks: ['app'],
      cache: true
    }),
    new HtmlWebPackPlugin({
      template: 'wwwsrc/admin.html',
      filename: './adm/index.html',
      favicon: 'wwwsrc/favicon.png',
      chunks: ['adm/index'],
      cache: true
    }),
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css'
    })
  ],
  optimization: {
    runtimeChunk: 'single', //single runtime bundle for all chunks
    //put node_modules and semantic into separate bundles
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all'
        },
        semantic: {
          test: /[\\/]semanticbuild[\\/]/,
          name: 'semantic',
          chunks: 'all'
        },
        libraries: {
          test: /[\\/]oliasoftDefaultLibraries[\\/]/,
          name: 'libraries',
          chunks: 'all'
        }
      }
    }
  },
  node
};
