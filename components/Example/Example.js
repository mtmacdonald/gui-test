import React from 'react';
import cx from 'classnames';
import styles from './Example.module.css';

export default class Example extends React.Component {
  render() {
    return (
      <div className={cx(styles.example)}>
        Example React Component
      </div>
    )
  }
}
