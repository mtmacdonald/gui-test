const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const getRules = require('../config/webpack.common.rules.js');
const node = require('../config/webpack.common.node.js');

module.exports = ({ config }) => {
  const rules = getRules('development');

  config.module.rules = []; //delete default storybook rules because they conflict (postcss causes a conflict)

  rules.forEach(r => {
    config.module.rules.push(r); //attach our own rules from the application webpack config
  });

  config.node = node;

  config.plugins.push(
    new MiniCssExtractPlugin({
      filename: '[name].css'
    })
  );

  config.resolve.alias = {
      '~common': path.resolve(__dirname, '../common/'),
      '~components': path.resolve(__dirname, '../client/components'),
      '~client': path.resolve(__dirname, '../client/'),
  };

  return config;
};
